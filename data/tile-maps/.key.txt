bg: (255,255,255)
spawn: (0,255,0)
endpoint: (0,240,0)
extra-swap: (0,0,128)
white-tile: (255,255,0)
white-ladderLEFT: (240,240,0)
white-ladderRIGHT: (230,230,0)
black-tile: (0,0,255)
black-ladderRIGHT: (0,0,250)
black-ladderLEFT: (0,0,240)
