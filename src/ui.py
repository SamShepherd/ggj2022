"""
Module for everything related to the UI
buttons, backgrounds, etc
"""
import pygame as p

import sound
import graphics as gfx
import globals as g

class Control:
    """
    Abstract class for controls. This should be inherited and not used directly
    """
    def __init__(self, rect, states):
        self.rect = rect
        #set of states where this control will be drawn and updated
        self.states = states
        #automatically convert single state string into set
        if type(self.states) == str:
            self.states = set((self.states,))

        self.deleted = False
        g.controls.append(self)

    def update(self):
        """
        Update the control
        Called every tick
        """
        pass

    def draw(self):
        """
        Basic drawing function, should be overridden
        """
        p.draw.rect(g.screen, "black", self.rect, 1)
        if self.rect.w >= 10 and self.rect.h >= 10:
            p.draw.rect(g.screen, "red", self.rect.inflate(-10, -10), 1)

    def delete(self):
        """
        Delete this control
        Inherit this as required but always call super().delete()
        """
        g.controls.remove(self)
        self.deleted = True

class Button(Control):
    """
    Class for buttons
    """
    def __init__(self, pos, command, states, unpressed_graphics, highlighted_graphics, pressed_graphics):

        

        #instruction to give when clicked
        self.command = command

        self.unpressed_graphics = unpressed_graphics
        self.highlighted_graphics = highlighted_graphics
        self.pressed_graphics = pressed_graphics

        rect = p.Rect(pos[0], pos[1], gfx.get_surface(self.unpressed_graphics).get_width(), gfx.get_surface(self.unpressed_graphics).get_height())

        self.pressed = False
        self.old_pressed = False
        self.highlighted = False

        super().__init__(rect, states)

    def update(self):
        super().update()

        self.pressed = False
        self.highlighted = False
        if self.rect.collidepoint((g.mx, g.my)):
            self.highlighted = True
            if g.ml:
                self.pressed = True            

        if self.pressed and not self.old_pressed:
            self.press()

        self.old_pressed = self.pressed
        
    def press(self):
        """
        Press the button
        Called when the button is clicked
        """
        sound.play("click")
        g.commands.append(self.command)

    def draw(self):
        if self.pressed:
            surface = gfx.get_surface(self.pressed_graphics)
        else:
            if self.highlighted:
                surface = gfx.get_surface(self.highlighted_graphics)
            else:
                surface = gfx.get_surface(self.unpressed_graphics)

        g.screen.blit(surface, self.rect)

class SwitchCount(Control):
    def __init__(self, pos, states):
        rect = p.Rect(pos[0], pos[1], 100, 50)
        super().__init__(rect, states)

        self.count_surfaces = g.spritesheets["tiles"].sprites[2][2:10]
        
    def draw(self):
        if g.player.switch_amount < len(self.count_surfaces):
            switch_index = g.player.switch_amount
        else:
            switch_index = len(self.count_surfaces)-1

        if switch_index:
            icon_surface = g.spritesheets["tiles"].sprites[2][1]
        else:
            icon_surface = g.spritesheets["tiles"].sprites[2][0]

        g.screen.blit(icon_surface, self.rect)

        surface = self.count_surfaces[switch_index]
        g.screen.blit(surface, self.rect.move(50,0))
    
class CoinCount(Control):
    def __init__(self, pos, font, states):
        rect = p.Rect(pos[0], pos[1], 100, 50)
        super().__init__(rect, states)

        self.icon = g.spritesheets["coin"].sprites[0][0]
        self.font = font

        self.text = "NONE"

    def draw(self):
        if g.current_level and (g.player.coins+g.current_level.collected_coins):
            old_text = self.text
            self.text = str(g.player.coins+g.current_level.collected_coins)
            if old_text != self.text:
                self.count_surface = self.font.render(self.text, False, "black")

            g.screen.blit(self.icon, self.rect)
            g.screen.blit(self.count_surface, self.rect.move(self.icon.get_width()+50, 0) )
    
