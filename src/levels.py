import pygame as p

import entities
import sound
import graphics as gfx
import globals as g

class Structure(entities.Entity):
    """
    Abstract class for structures
    """
    def __init__(self, x, y, w, h, level, static=True):
        rect = p.Rect(x*g.TW, y*g.TH, w*g.TW, h*g.TH)
        self.original_rect = rect.copy()
        self.level = level
        super().__init__(rect, static=static)

        self.level.structures.append(self)

class StaticSprite(Structure):
    def __init__(self, x, y, graphics, level):
        super().__init__(0,0,0,0, level, static=True)
        self.graphics = graphics
        self.rect = gfx.get_surface(self.graphics).get_rect().move(x,y)
        self.update_from_rect()
        
    def update_tick(self):
        if type(self.graphics) == gfx.Animation:
            self.graphics.update()

    def draw(self):
        g.screen.blit(gfx.get_surface(self.graphics), self.rect)

class Checkpoint(Structure):
    """
    Class for game checkpoints
    """
    def __init__(self, x, y, level):
        super().__init__(x, y, 1, 1, level, static=True)
        self.surface_offset = 0

        self.surface = g.spritesheets["tiles"].sprites[3][0]
        self.scroll = 0
        self.scroll_speed = 0.5

    def update_tick(self):
        super().update_tick()
        if self.rect.colliderect(g.player.rect):
            #go to next level
            sound.play("finish")
            g.commands.append("next_level")

        self.scroll += self.scroll_speed
        while self.scroll >= self.surface.get_width():
            self.scroll -= self.surface.get_width()

    def draw(self):
        g.screen.blit(self.surface, self.rect.move(self.scroll,0), p.Rect(0, 0, self.rect.w-self.scroll, self.rect.h+1))
        g.screen.blit(self.surface, self.rect, p.Rect(self.rect.w-self.scroll, 0, self.rect.w, self.rect.h+1))

class ExtraSwitch(Structure):
    """
    Class for extra switch pickups
    """
    def __init__(self, x, y, level):
        super().__init__(x, y, 1, 1, level, static=True)
        self.active = True

        self.animation = g.spritesheets["extra_switch"].create_animation(0, 16)

    def update_tick(self):
        super().update_tick()
        if self.active and self.rect.colliderect(g.player.rect):
            #pickup
            sound.play("pickup")
            g.player.switch_amount += 1
            gfx.Overlay((0,200,255), 40, 80)
            self.active = False

        self.animation.update()

    def draw(self):
        if self.active:
            g.screen.blit(self.animation.get_frame(), self.rect)

    def delete(self):
        super().delete()
        g.current_level.structures.remove(self)

class Coin(Structure):
    """
    Class for coin pickups
    """
    def __init__(self, x, y, level):
        super().__init__(x, y, 1, 1, level, static=True)
        self.active = True

        self.animation = g.spritesheets["coin"].create_animation(0, 16)

    def update_tick(self):
        super().update_tick()
        if self.active and self.rect.colliderect(g.player.rect):
            #pickup
            sound.play("coin")
            gfx.Overlay((255,255,0), 40, 80)
            self.active = False
            self.level.collected_coins += 1

        self.animation.update()

    def draw(self):
        if self.active:
            g.screen.blit(self.animation.get_frame(), self.rect)

    def delete(self):
        super().delete()
        g.current_level.structures.remove(self)

class Tile:
    """
    A single instance of a tile
    """
    def __init__(self, name, pos, tile_type, tile_mode):
        """
        name: name of tile
        pos: 2-tuple representing the position of the tile in the tilemap
        tile_type: e.g. "ladder", "block"
        tile_mode: 0 or 1 or None
        """
        self.name = name #not used rn
        self.tile_type = tile_type
        self.tile_mode = tile_mode

        self.solid = False
        if self.tile_type == "block":
            self.solid = True

        self.is_ladder = False
        if self.tile_type == "ladder":
            self.is_ladder = True

        self.is_wall = False
        if self.tile_type == "wall":
            self.is_wall = True

        self.rect = p.Rect(pos[0]*g.TW, pos[1]*g.TH, g.TW, g.TH)

        self.surf_coords = {
            "background":(9,9),

            "white-tile":(0,0),
            "white-tile2":(0,3),

            "white-ladderLEFT":(0,1),
            "white-ladderRIGHT":(0,2),

            "white-wall":(5,0),

            "black-tile":(1,0),
            "black-tile2":(1,3),

            "black-ladderLEFT":(1,1),
            "black-ladderRIGHT":(1,2),

            "black-wall":(5,1)
        }[self.name]

    def draw(self, surf=None):
        if not surf:
            surf = g.screen

        surface = g.spritesheets["tiles"].sprites[ self.surf_coords[0] ][ self.surf_coords[1] ]
        #set alpha based on whether thing is active or not
        if self.tile_mode is 0:
            surface.set_alpha(50)
        else:
            surface.set_alpha(255)
        surf.blit(surface, self.rect)
        surface.set_alpha(255)

class Segment:
    """
    Single segment of the level, which is flippable
    """
    def __init__(self, level, rect):
        self.rect = rect
        self.sx = self.rect.left//g.TW
        self.sy = self.rect.top//g.TH
        self.ex = self.rect.right//g.TW
        self.ey = self.rect.bottom//g.TH

        #bounds settings
        self.bounds_line_size = 10
        self.bounds_scroll = 0
        self.bounds_scroll_speed = 0.1
        self.bounds_width = 2

        #textbox
        rect = p.Rect(0,0,50,50)
        rect.midbottom = self.rect.midbottom
        rect.y -= 25

        #self.textbox = entities.TextBox(rect, str(len(level.segments)+1), g.fonts["font2-1"])
        self.surface = g.spritesheets["tiles"].sprites[4][len(level.segments)]
        #level.structures.append(self.textbox)

        level.segments.append(self)

        self._set_switched(False)

    def update_tick(self):
        self.bounds_scroll = (self.bounds_scroll+self.bounds_scroll_speed)%(self.bounds_line_size*2)

    def draw(self):
        """
        Draw graphics relating to this segment
        """

        #draw bounds
        if self.switched:
            y = -self.bounds_line_size+self.bounds_scroll
        else:
            y = -self.bounds_scroll

        while y < g.HEIGHT:
            if self.switched:
                colour = "white"
            else:
                colour = "black"

            p.draw.line(g.screen, colour, (self.rect.right-(self.bounds_width//2),y), (self.rect.right-(self.bounds_width//2),y+self.bounds_line_size), width=self.bounds_width)
            p.draw.line(g.screen, colour, (self.rect.left+(self.bounds_width//2),y), (self.rect.left+(self.bounds_width//2),y+self.bounds_line_size), width=self.bounds_width)
            y += self.bounds_line_size*2
        
        #draw number
        rect = p.Rect(0,0,32,32)
        rect.midbottom = self.rect.midbottom
        rect.top -= 50
        g.screen.blit(self.surface, rect)

    def _set_switched(self, val):
        """
        Set switched value
        """
        self.switched = val
        #if self.switched:
        #    self.textbox.background_colour = (64, 64, 64, 128)
        #else:
        #   self.textbox.background_colour = (196, 196, 196, 128)

    def flip(self):
        """
        Flip all tiles within this segment
        """

        #change colours
        self._set_switched(not self.switched)

        #flip tiles
        for x in range(self.sx, self.ex):
            for y in range(self.sy, self.ey):
                tile = g.current_level.tiles[x][y]
                if tile.tile_mode is 0:
                    tile.tile_mode = 1
                elif tile.tile_mode is 1:
                    tile.tile_mode = 0

class Level:
    """
    A whole level
    """
    def __init__(self, data_path, segment_amount, switch_amount, tile_key):
        """
        data_path: the path to the level data image
        tile_key: dictionary used to connect pixel colours to tiles
        """

        self.name = data_path
        self.spawn_x = 0
        self.spawn_y = 0
        self.load_from_file(data_path, tile_key)

        self.collected_coins = 0

        self.switch_amount = switch_amount

        #create segments
        self.segments = []
        if segment_amount:
            segment_amount = segment_amount
            segment_width = (self.width*g.TW)//segment_amount
            for i in range(segment_amount):
                rect = p.Rect(i*segment_width, 0, segment_width, self.height*g.TH)
                Segment(self, rect)

    def update_tick(self):
        """
        Update level
        Called every tick
        """
        if not g.player.rect.colliderect(self.rect):
            g.commands.append("respawn_player")

    def spawn_player(self):
        g.player.spawn_at(self.spawn_x, self.spawn_y)
        g.player.switch_amount = self.switch_amount

        self.collected_coins = 0

        for structure in self.structures:
            if hasattr(structure, "active"):
                structure.active = True

        for segment in self.segments:
            if segment.switched:
                segment.flip()

    def load_from_file(self, path, tile_key):
        self.tiles = []
        self.structures = []

        """
        Load a file from an image at the given path
        """
        path = g.LEVELS_DIR+path
        print("loading:",path)

        level_surface = p.image.load(path)

        #build tiles from image
        black = (0,0,0)
        white = (255,255,255)
        for x in range(level_surface.get_width()):
            self.tiles.append([])
            for y in range(level_surface.get_height()):
                colour = level_surface.get_at((x,y))[:3]

                tile_data = tile_key.get(colour)
                

                #non-tile objects
                if type(tile_data) == str:
                    if tile_data == "spawn":
                        self.spawn_x = x
                        self.spawn_y = y-(g.player.rect.height/2//g.TH)
                    elif tile_data == "endpoint":
                        Checkpoint(x, y, self)
                    elif tile_data == "extra-switch":
                        ExtraSwitch(x, y, self)
                    elif tile_data == "coin":
                        Coin(x, y, self)

                #if colour cannot be found in key
                if not type(tile_data) == dict:
                    tile_data = {"name":"background", "type":"background", "mode":None}

                #create tile and add to level
                new_tile = Tile(tile_data["name"], (x,y), tile_data["type"], tile_data["mode"])

                self.tiles[-1].append(new_tile)

        self.width = level_surface.get_width()
        self.height = level_surface.get_height()
        self.rect = p.Rect(0, 0, self.width*g.TW, self.height*g.TH)

    def draw_all(self, surf=None):
        """
        Draw all tiles in a level
        """
        if not surf:
            surf = g.screen

        for column in self.tiles:
            for tile in column:
                tile.draw(surf=surf)

    def draw_segments(self):
        """
        Draw all the graphics relating to the segments
        """
        for segment in self.segments:
            segment.draw()

    def delete(self):
        """
        Delete and cleanup level
        """
        for structure in self.structures[:]:
            structure.delete()
