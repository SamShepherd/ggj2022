import pygame as p
from sys import exit as sys_exit

import levels
import entities
import creatures
import ui
import sound
import utilities as util
import graphics as gfx
import globals as g

#setup display
g.screen = p.display.set_mode((g.WIDTH, g.HEIGHT)) #p.FULLSCREEN|p.SCALED)
p.display.set_caption("Night Mode")

icon = gfx.li("icon.png")
icon_switched = gfx.li("icon_switched.png")
g.current_icon = icon
p.display.set_icon(g.current_icon)

end_screen = gfx.li("end_screen.png")
background = gfx.li("background.png")
intro = gfx.li("intro.png")
max_intro_timer = 300
intro_timer = max_intro_timer

#setup sounds
p.mixer.init()
sound.load_all()

#setup level key
tile_key = {
    (255,255,255):{"name":"background", "type":"background", "mode":None},

    (255,255,0):{"name":"white-tile", "type":"block", "mode":1},
    (254,254,0):{"name":"white-tile2", "type":"block", "mode":1},

    (240,240,0):{"name":"white-ladderLEFT", "type":"ladder", "mode":1},
    (230,230,0):{"name":"white-ladderRIGHT", "type":"ladder", "mode":1},

    (200,200,0):{"name":"white-wall", "type":"wall", "mode":1},



    (0,0,255):{"name":"black-tile", "type":"block", "mode":0},
    (0,0,254):{"name":"black-tile2", "type":"block", "mode":0},

    (0,0,250):{"name":"black-ladderLEFT", "type":"ladder", "mode":0},
    (0,0,240):{"name":"black-ladderRIGHT", "type":"ladder", "mode":0},

    (0,0,200):{"name":"black-wall", "type":"wall", "mode":0},

    (0,255,0):"spawn",
    (0,240,0):"endpoint",
    (0,0,128):"extra-switch",
    (10,10,10):"coin"
}
level_order = [
    {"path":"map-1.png", "segments":0, "switches":7, "text":[( ("tut1-1.png","tut1-2.png"), 142, 256)] },
    {"path":"map-2.png", "segments":0, "switches":7, "text":[( ("tut2-1.png","tut2-2.png"), 740, 430)] },
    {"path":"map-3.png", "segments":2, "switches":7, "text":[( ("tut3-1.png","tut3-2.png"), 325, 101)] },
    {"path":"map-4.png", "segments":3, "switches":7, "text":[( ("tut4-1.png","tut4-2.png"), 470, 300)] },
    {"path":"map-5.png", "segments":4, "switches":7, "text":[] },
    {"path":"map-6.png", "segments":4, "switches":7, "text":[] },
    {"path":"map-7.png", "segments":4, "switches":7, "text":[] },
    {"path":"map-8.png", "segments":4, "switches":7, "text":[] },
    "end"
]

#graphics
gfx.Spritesheet("player", "player_ss.png", 65, 108)
gfx.Spritesheet("tiles", "tile_ss.png", 32, 32)
gfx.Spritesheet("extra_switch", "extra_switch_ss.png", 32, 32)
gfx.Spritesheet("coin", "coin_ss.png", 32, 32)
p.font.init()
g.fonts.update({
    "font1-1":p.font.SysFont("consolas", 40),
    "font2-1":p.font.SysFont("consolas", 40)
})

def go_to_next_level():
    g.sound_dict["move"].stop()
    
    #add coins
    if g.current_level:
        for structure in g.current_level.structures:
            if type(structure) == levels.Coin and not structure.active:
                g.player.coins += 1

    #go to next level
    if not g.current_level:
        next_level_index = 0
    else:
        #transition effect
        g.old_level_gfx = p.Surface((g.WIDTH, g.HEIGHT))
        g.old_level_gfx.fill(g.BACKGROUND_COLOUR)
        g.current_level.draw_all(surf=g.old_level_gfx)
        g.level_transition = g.WIDTH
        g.current_state = set(("transition",))

        #destroy and cleanup current level
        g.current_level.delete()

        i = 0
        current_level_index = i
        for level_data in level_order:
            if level_data["path"] == g.current_level.name:
                
                current_level_index = i
                break
            i += 1

        if current_level_index+1 < len(level_order):
            next_level_index = current_level_index+1
        else:
            next_level_index = 0

    level_data = level_order[next_level_index]
    load_level_from_data(level_data)

def reload_current_level():
    """
    Reload the current level (from raw data)
    """
    i = 0
    current_level_index = i
    for level_data in level_order:
        if level_data["path"] == g.current_level.name:
            
            current_level_index = i
            break
        i += 1
    level_data = level_order[current_level_index]
    load_level_from_data(level_data)

def load_level_from_data(level_data):
    """
    Set current level from given data
    """
    #create level
    if level_data == "end":
        #end game
        g.current_state = set(("end",))
    else:
        g.current_level = levels.Level(level_data["path"], level_data["segments"], level_data["switches"], tile_key)

        #transition effect
        g.new_level_gfx = p.Surface((g.WIDTH, g.HEIGHT))
        g.new_level_gfx.fill(g.BACKGROUND_COLOUR)
        g.current_level.draw_all(surf=g.new_level_gfx)

        #create text
        for text_data in level_data["text"]:
            surfaces = [gfx.li(dat) for dat in text_data[0]]
            anim = gfx.Animation(surfaces, 30)

            levels.StaticSprite(text_data[1], text_data[2], anim, g.current_level)

        g.current_level.spawn_player()

        #create overlay
        gfx.Overlay((0,255,0), 100, 60)
    

def switch_segment(segment):
    """
    Switch the tiles of a segment
    """
    sound.play("switch")
    segment.flip()
    g.player.switch_amount -= 1

    #switch icon
    if g.current_icon == icon:
        g.current_icon = icon_switched
    else:
        g.current_icon = icon
    p.display.set_icon(g.current_icon)

def handle_input(delta_tick):
    """
    Handle input like keys, etc
    """

    #update mouse
    g.mx, g.my = p.mouse.get_pos()
    g.ml, g.mm, g.mr = p.mouse.get_pressed()

    keys = p.key.get_pressed()
    for event in p.event.get():
        if event.type == p.QUIT:
            p.display.quit()
            sys_exit()

        elif event.type == p.KEYDOWN:
            if event.key == p.K_SPACE:
                if g.player.fall_time < g.player.fall_jump_tolerance and not g.player.climbing:
                    g.player.jump()
                if g.player.climbing:
                    g.player.climbing = False

            elif event.key == p.K_q or event.key == p.K_e:
                if g.current_level.segments:
                    index = int(g.player.rect.centerx/g.WIDTH*len(g.current_level.segments))
                    if index < len(g.current_level.segments):
                        switch_segment(g.current_level.segments[index])

            elif event.key == p.K_r:
                reload_current_level()

            elif p.K_1 <= event.key <= p.K_9:
                index = event.key-p.K_1
                if index < len(g.current_level.segments):
                    if g.player.switch_amount:
                        switch_segment(g.current_level.segments[index])
                        

                    #if util.check_collision(g.player.rect, g.player.collision_dict):
                    #    g.commands.append("respawn_player")

    if not g.player.climbing or g.player.is_grounded:
        if keys[p.K_d]:
            if abs(g.player.vx) < 0.01:
                g.sound_dict["move"].stop()
                g.sound_dict["move"].play(loops=-1)
                #sound.play("start")

            g.player.vx += g.player.speed_increase
            g.player.direction = "right"

        if keys[p.K_a]:
            if abs(g.player.vx) < 0.01:
                g.sound_dict["move"].stop()
                g.sound_dict["move"].play(loops=-1)
                #sound.play("start")

            g.player.vx -= g.player.speed_increase
            g.player.direction = "left"

            

    #enable/disable acceleration
    if keys[p.K_d] or keys[p.K_a]:
        g.player.vx_loss = 0
    else:
        g.player.vx_loss = g.player.normal_vx_loss

    if abs(g.player.vx) >= g.player.max_speed:
        g.player.vx = g.player.max_speed * (g.player.vx/abs(g.player.vx))

    if keys[p.K_w] or keys[p.K_s] or keys[p.K_SPACE]:
        if g.player.is_on_ladder and not g.player.climb_cooldown:
            g.player.climbing = util.check_collision(g.player.rect, {"ladder":True})
            g.sound_dict["move"].stop()
            g.player.climb_cooldown = g.player.max_climb_cooldown

            if "LEFT" in g.player.climbing.name:
                g.player.rect.right = g.player.climbing.rect.right
            elif "RIGHT" in g.player.climbing.name:
                g.player.rect.left = g.player.climbing.rect.left

            x_frac = g.player.x%1
            y_frac = g.player.y%1
            g.player.update_from_rect()
            g.player.x += x_frac
            g.player.y += y_frac


        if g.player.climbing:
            if keys[p.K_w]:
                g.player.move(0, -g.player.climb_speed*delta_tick)
            if keys[p.K_s]:
                g.player.move(0, g.player.climb_speed*delta_tick)

            if not g.player.is_on_ladder:
                if "LEFT" in g.player.climbing.name:
                    g.player.move(30, -20)
                elif "RIGHT" in g.player.climbing.name:
                    g.player.move(-30, -20)

    if g.player.climbing:
        if not g.player.is_on_ladder:
            g.player.climbing = False
        g.player.vx = 0
        g.player.vy = 0

#setup UI
ui.Button((911, 328), "start_game", "start_menu", gfx.li("play.png"), gfx.li("play.png"), gfx.li("play.png"))
ui.Button((953, 506), "exit_game", "start_menu", gfx.li("exit.png"), gfx.li("exit.png"), gfx.li("exit.png"))
ui.Button((450,450), "restart_game", "end", gfx.li("restart.png"), gfx.li("restart.png"), gfx.li("restart.png"))
ui.SwitchCount(p.Rect(50,0,200,100), "main")
ui.CoinCount(p.Rect(100,0,200,100), g.fonts["font1-1"], "main")

#g.fps_text_box = entities.TextBox(p.Rect(0,0,100,50), "FPS", g.fonts["font1-1"])

g.current_state = set(("start_menu",))

RUNNING = True
game_clock = p.time.Clock()

#setup timings
delta = 0
time_since_last_tick = 0
game_clock.tick()

g.player = creatures.Player()
g.player.reset()
go_to_next_level()

while RUNNING:
    g.screen.fill( g.BACKGROUND_COLOUR)

    delta = game_clock.tick()
    if delta > 1000:
        delta = 0

    #how many ticks have elapsed
    delta_tick = delta/(1000/g.TICK_RATE)

    time_since_last_tick += delta_tick
    ticks_to_perform = int(time_since_last_tick)
    time_since_last_tick -= ticks_to_perform

    if intro_timer:
        intro_timer -= 1

    #print(delta, delta_tick, time_since_last_tick, ticks_to_perform)

    #handle commands
    switched_level = False
    for command in g.commands:
        if command == "start_game":
            g.current_state = set(("main",))

        elif command == "end_game":
            p.display.quit()
            sys_exit()

        elif command == "next_level" and not switched_level:
            go_to_next_level()
            switched_level = True

        elif command == "respawn_player":
            g.current_level.spawn_player()
            gfx.Overlay((255,0,0), 100, 60)
            sound.play("die")

        elif command == "restart_game":
            g.current_state = set(("start_menu",))
            g.current_level = None
            go_to_next_level()
            g.player.reset()

    g.commands.clear()

    #handle level transition
    if "transition" in g.current_state:
        if g.level_transition <= 0:
            g.current_state = set(("main",))
        else:
            g.level_transition -= time_since_last_tick*g.level_transition_speed 

        g.screen.blit(g.old_level_gfx, (-g.WIDTH+g.level_transition, 0))
        g.screen.blit(g.new_level_gfx, (g.level_transition, 0))
        g.screen.blit(g.player.anim_system.get_frame(), (g.player.pre_spawn_x-g.WIDTH+g.level_transition, g.player.pre_spawn_y))
        #print(g.level_transition, g.WIDTH-g.level_transition, (g.WIDTH*2)-g.level_transition)

    else:
        #deal with input
        handle_input(delta_tick)

        #handle animations
        if ticks_to_perform:
            for animation_system in g.animation_systems:
                animation_system.update()
        #handle UI
        if ticks_to_perform:
            i = 0
            while i < len(g.controls):
                control = g.controls[i]
                if not control.states.isdisjoint(g.current_state):
                    control.update()

                if not control.deleted:
                    i += 1
        #handle overlays
        for overlay in g.overlays:
            overlay.update_tick()

        #main game state
        if "main" in g.current_state:
            #update everything

            #update level
            for tick in range(ticks_to_perform):
                g.current_level.update_tick()
                for segment in g.current_level.segments:
                    segment.update_tick()

            #can't use a for loop since the list could change size during the loop
            i = 0
            while i < len(g.entities):
                entity = g.entities[i]
                entity.update(delta_tick)

                for tick in range(ticks_to_perform):
                    entity.update_tick()

                if not entity.deleted:
                    i += 1

            #draw level
            #TODO - optimise this
            g.current_level.draw_all()
            g.current_level.draw_segments()

            for structure in g.current_level.structures:
                structure.draw()

            #draw creatures (just player for now)
            g.player.draw()

        if "start_menu" in g.current_state:
            g.screen.blit(background, (0,0))

        if "end" in g.current_state:
            g.screen.blit(end_screen, (0,0))

        for control in g.controls:
            if not control.states.isdisjoint(g.current_state):
                control.draw()

    

    if "main" in g.current_state or "transition" in g.current_state:
        #update text box
        if g.fps_text_box:
            g.fps_text_box.set_text(str(round(game_clock.get_fps())))
        if g.fps_text_box:
            g.fps_text_box.draw()

    if not "transition" in g.current_state:
        for overlay in g.overlays:
            overlay.draw()

    if intro_timer:
        intro.set_alpha( 255* (intro_timer/max_intro_timer) )
        g.screen.blit(intro, (0,0))

    p.display.flip()