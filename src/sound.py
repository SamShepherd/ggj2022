import pygame as p
import os

import globals as g

def load_all():
    for file in os.listdir(g.SOUND_DIR):
        print(file)
        g.sound_dict.update({file[:-4] : p.mixer.Sound(g.SOUND_DIR+file) })

def play(name):
    g.sound_dict[name].play()