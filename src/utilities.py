"""
Module for holding general purpose useful functions
"""

import pygame as p

import globals as g

def clamp(val, min_val, max_val):
    """
    Clamp a value between a given minimum and maximum
    """
    return min(max(val, min_val), max_val)

def check_collision(rect, collision_dict):
    """
    Check to see if a given rect is colliding with anything, given a collision dict
    Returns first thing that collision is detected with, or False if there is no collision
    """
    colliding = False
    if not collision_dict: #entity cannot collide
        return colliding

    if collision_dict.get("level") or collision_dict.get("ladder") or collision_dict.get("level_wall"):
        #see what tiles we need to check
        sx = int(rect.x//g.TW)-1
        sy = int(rect.y//g.TH)-1
        ex = int(rect.right//g.TH)+1
        ey = int(rect.bottom//g.TH)+1

        #bounds check/clamp
        if sx < g.current_level.width and sy < g.current_level.height and ex > 0 and ey > 0:
            sx = clamp(sx, 0, g.current_level.width)
            sy = clamp(sy, 0, g.current_level.height)
            ex = clamp(ex, 0, g.current_level.width)
            ey = clamp(ey, 0, g.current_level.height)

            #check each tile in range
            for x in range(sx,ex):
                for y in range(sy,ey):
                    tile = g.current_level.tiles[x][y]
                    if (tile.solid and tile.tile_mode is not 0 and collision_dict.get("level")) or \
                    (tile.is_ladder and tile.tile_mode is not 0 and collision_dict.get("ladder")) or \
                    (tile.is_wall and tile.tile_mode is not 0 and collision_dict.get("level_wall")):
                        if rect.colliderect(tile.rect):
                            colliding = tile
                            return colliding

    return colliding



