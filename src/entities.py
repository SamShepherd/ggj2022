import pygame as p
import pygame.gfxdraw as pg

import utilities as util
import globals as g

class Entity:
    """
    Abstract entity class for basically anything that isn't just a tile. Should be inherited and not instanced directly
    """
    def __init__(self, rect, **_kwargs):
        self.rect = rect
        #need x and y attributes for floating point precision since rects only work with integers
        self.x = self.rect.x
        self.y = self.rect.y

        #velocity stuff
        self.vx = 0
        self.vy = 0
        self.vx_loss = 0.01
        self.vy_loss = 0.01
        self.max_vx = 10
        self.max_vy = 15
        self.apply_gravity = True
        self.static = False

        #what things the entity will collide with
        self.collision_dict = {"level":True}

        #whether this entity has been deleted or not
        self.deleted = False

        #apply custom kwargs
        kwargs = {}
        kwargs.update(_kwargs)
        self.__dict__.update(kwargs)

        g.entities.append(self)

    def update(self, delta_tick):
        """
        Update this entity
        Called as often as possible
        delta: time change in ms since this was last called (roughly)
        """

        #apply velocity to move the entity
        if not self.static:
            vx = self.vx*delta_tick
            vy = self.vy*delta_tick


            mx, my = self.move(vx, vy)

            if not mx:
                self.vx = 0
            if not my:
                self.vy = 0

        #print(util.check_collision(self.rect, self.collision_dict))

        self.update_rect()

    def update_tick(self):
        """
        Update this entities g.TICK_RATE times a second
        """
        if not self.static:
            if self.vx:
                old_vx = self.vx
                self.vx -= self.vx_loss*(self.vx/abs(self.vx))
                if (old_vx < 0 and self.vx > 0) or (old_vx > 0 and self.vx < 0):
                    self.vx = 0

            if self.vy:
                old_vy = self.vy
                self.vy -= self.vy_loss*(self.vy/abs(self.vy))
                if (old_vy < 0 and self.vy > 0) or (old_vy > 0 and self.vy < 0):
                    self.vy = 0

            if self.apply_gravity:
                self.vy += g.GRAV

            if abs(self.vx) >= self.max_vx:
                self.vx = abs(self.max_vx)/self.max_vx
            if abs(self.vy) >= self.max_vy:
                self.vy = abs(self.max_vy)/self.max_vy

            if abs(self.vx) <= 0.001:
                self.vx = 0
            if abs(self.vy) <= 0.001:
                self.vy = 0
            #print(self.vx, self.vy)

    def move(self, vx, vy):
        """
        Move the entity, checking for collision
        """
        collide_x = False
        collide_y = False

        #exit early if no movement
        if not vx and not vy:
            return True, True

        if vy > 0:
            collision_check_rect = p.Rect(self.x, self.y+vy, self.rect.w, 2)
            collision_check_rect.y += self.rect.height-1
            

            colliding = util.check_collision(collision_check_rect, self.collision_dict)

            if colliding:
                collide_y = True
                if hasattr(colliding, "rect"):
                    self.y = colliding.rect.top-self.rect.height + (self.y%1)
                self.vy = 0

        self.update_rect()

        collision_check_rect = self.rect.move(vx, 0)
        colliding = util.check_collision(collision_check_rect, {"level_wall":True})
        if colliding:
            collide_x = True
            if hasattr(colliding, "rect"):
                if self.vx >= 0:
                    self.x = colliding.rect.left-self.rect.width
                else:
                    self.x = colliding.rect.right
            self.vx = 0
            


        if not collide_x:
            self.x += vx
        if not collide_y:
            self.y += vy

        self.update_rect()

        return not collide_x, not collide_y

    @property
    def is_grounded(self):
        rect = p.Rect(self.rect.x,0,self.rect.w,1)
        rect.bottom = self.rect.bottom+1
        return util.check_collision(rect, self.collision_dict)

        
    def update_from_rect(self, rect=None):
        """
        Sync this entities' position with it's rect (or an optional rect that can be passed in)
        """
        if not rect:
            rect = self.rect
        self.x = rect.x
        self.y = rect.y
        self.update_rect()

    def update_rect(self):
        """
        Sync this entities' rect position with it's position
        """
        self.rect.x = self.x
        self.rect.y = self.y

    def draw(self, colour="red"):
        """
        Basic draw function, should be overridden by anything inheriting this class
        """
        p.draw.rect(g.screen, colour, self.rect)

    def delete(self):
        """
        Delete this entity
        Inherit this as required but always call super().delete()
        """
        g.entities.remove(self)
        self.deleted = True

class TextBox(Entity):
    def __init__(self, rect, text, font, text_colour="black", border_colour="black", border_thickness=4, background_colour=(196,196,196)):
        super().__init__(rect, static=True)

        self.font = font
        self.text = None

        self.text_colour = text_colour
        self.border_colour = border_colour
        self.border_thickness = border_thickness
        self.background_colour = background_colour

        self.set_text(text)

    def set_text(self, text):
        """
        Set the text of this text box
        """
        if self.text != text:
            self.text = text
            self.text_surface = self.font.render(self.text, False, self.text_colour)
            self.text_width = self.text_surface.get_width()
            self.text_height = self.text_surface.get_height()

    def draw(self):
        """
        Draw the text box
        """

        pg.box(g.screen, self.rect.inflate(-self.border_thickness, -self.border_thickness), self.background_colour)
        p.draw.rect(g.screen, self.border_colour, self.rect, self.border_thickness, 5)
        x, y = self.rect.center
        x -= self.text_width//2
        y -= self.text_height//2

        
        g.screen.blit(self.text_surface, (x, y))

        