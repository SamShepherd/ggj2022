"""
Holds all constants used across modules
"""

import os

#directory
#DATA_DIR = ".."+os.path.sep+"data"+os.path.sep
DATA_DIR = "data"+os.path.sep
LEVELS_DIR = DATA_DIR
GFX_DIR = DATA_DIR
SOUND_DIR = DATA_DIR+"sounds"+os.path.sep

#display
WIDTH = 1280
HEIGHT = 640
screen = None
current_icon = None
BACKGROUND_COLOUR = (180, 180, 180)

#state
current_state = set("menu")
commands = []

#graphics
spritesheets = {}
animation_systems = []
fonts = {}
overlays = []

old_level_gfx = None
new_level_gfx = None
level_transition = 0
level_transition_speed = 2

#sound
sound_dict = {}

#ui
mx, my = 0, 0 #mouse pos
ml, mm, mr = False, False, False #mouse buttons
controls = []
fps_text_box = None

#levels
TW = 32 #horizontal pixels per tile
TH = 32 #vertical pixels per tile
current_level = None
GRAV = 0.75

#entities
TICK_RATE = 60
entities = []
player = None