"""
Module for handling graphics and animation
"""

import pygame as p

import globals as g

def get_surface(graphics):
    """
    Get a pygame Surface object from various types of graphical data
    """

    #if graphics are already a Surface
    if type(graphics) == p.Surface:
        return graphics

    elif type(graphics) == Animation:
        return graphics.get_frame()

def li(path):
    """
    Shortcut function for loading an image
    """
    return p.image.load(g.GFX_DIR+path)

class Overlay:
    """
    Class for screen overlays
    """
    def __init__(self, colour, start_alpha, ticks):
        self.surface = p.Surface((g.WIDTH, g.HEIGHT))
        self.surface.fill(colour)

        self.alpha_change = start_alpha/ticks
        self.alpha = start_alpha
        self.surface.set_alpha(self.alpha)

        g.overlays.append(self)

    def update_tick(self):
        self.alpha -= self.alpha_change
        self.surface.set_alpha(self.alpha)
        if self.alpha <= 0:
            g.overlays.remove(self)

    def draw(self):
        g.screen.blit(self.surface, (0,0))

class Animation:
    """
    Class for handling animations
    """
    def __init__(self, surfaces, ticks_per_frame):
        self.surfaces = surfaces
        self.frame = 0
        self.ticks_per_frame = ticks_per_frame

        
    def update(self):
        """
        Update animation frame if required
        Called every tick
        """
        self.frame = (self.frame+(1/self.ticks_per_frame)) % len(self.surfaces)

    def get_frame(self):
        return self.surfaces[int(self.frame)]

class Animation_System:
    """
    Class for holding a collection of animations
    Where only one is being played at once
    """

    def __init__(self, anim_dict):
        self.anim_dict = anim_dict
        self.playing_animation = None

        g.animation_systems.append(self)

    def update(self):
        """
        Update currently playing animation
        Called every tick
        """
        anim = self.anim_dict.get(self.playing_animation)
        if anim:
            anim.update()

    def set_playing(self, name):
        """
        Set which animation is playing
        """
        anim = self.anim_dict.get(name)
        if anim:
            #reset old animation
            old_anim = self.anim_dict.get(self.playing_animation)
            if old_anim and self.playing_animation != name:
                old_anim.frame = 0

            self.playing_animation = name

    def get_playing(self):
        """
        Get the animation which is currently playing
        """
        return self.anim_dict.get(self.playing_animation)

    def get_frame(self):
        """
        Get the current frame of the currently playing animation
        """
        anim = self.anim_dict.get(self.playing_animation)
        if anim:
            return anim.get_frame()

    def delete(self):
        """
        Delete this animation system
        """
        g.animation_systems.remove(self)
        print("system deleted,", len(g.animation_systems), "remain")

class Spritesheet:
    """
    Class for handling loading multiple sprites or creating animations from a single image file
    """
    def __init__(self, name, path, sprite_width, sprite_height):

        self.name = name

        path = g.GFX_DIR+path
        self.load_from_file(path, sprite_width, sprite_height)

        #cache so that animations can be created more easily
        self.animation_cache = {}

        g.spritesheets.update({self.name:self})

    def load_from_file(self, path, sprite_width, sprite_height):
        """
        Load spritesheet from image at given path
        """
        self.surface = p.image.load(path)
        self.sprite_width = sprite_width
        self.sprite_height = sprite_height

        self.sprites = []
        for y in range( int(self.surface.get_height()//self.sprite_height) ):
            self.sprites.append([])
            for x in range( int(self.surface.get_width()//self.sprite_width) ):
                sprite_surface = p.Surface((self.sprite_width, self.sprite_height), flags=p.SRCALPHA)
                sprite_surface.blit(self.surface, (-(x*self.sprite_width), -(y*self.sprite_height)) )
                self.sprites[-1].append(sprite_surface)

    def create_animation(self, anim_index, ticks_per_frame):
        """
        Create or load an animation with a given index into the spritesheet and speed
        """
        anim_info_tuple = (anim_index, ticks_per_frame)

        #check cache
        anim = self.animation_cache.get(anim_info_tuple)

        if not anim:
            print("no anim for spritesheet",self.name,anim_index)
            #create new animation
            frames = self.sprites[anim_index]
            anim = Animation(frames, ticks_per_frame)
            #add to cache
            self.animation_cache[anim_info_tuple] = anim

        return anim
