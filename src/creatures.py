"""
Module for creatures
Just the player for now... maybe other things in future if needed
"""

import pygame as p

import entities
import sound
import graphics as gfx
import utilities as util
import globals as g

class Creature(entities.Entity):
    """
    Abstract creature class. Should be inherited and not instanced directly
    """
    def __init__(self, rect):
        super().__init__(rect)

    def draw(self, colour="red"):
        """
        Basic draw function, should be overridden by anything inheriting this class
        """
        p.draw.rect(g.screen, colour, self.rect)

    def delete(self):
        """
        Delete this creature
        Inherit this as required but always call super().delete()
        """
        super().delete()

class Player(Creature):
    """
    Controllable player entity, should only ever be one of these
    """
    def __init__(self):

        #is the player climbing a ladder
        self.climbing = False

        rect = p.Rect(0, 0, 65, 108)

        super().__init__(rect)

        #horizontal movement of player per tick
        self.speed_increase = 0.15
        self.max_speed = 6

        self.normal_vx_loss = 1
        self.normal_vy_loss = self.vy_loss

        self.climb_speed = 4
        self.max_climb_cooldown = 30
        self.climb_cooldown = self.max_climb_cooldown
        self.jump_strength = 11
        self.fall_jump_tolerance = 5 #how many ticks the player can still jump after falling

        self.fall_time = 0
        self.direction = "right"

        self.max_vx = 1000
        self.max_vy = 50

        self.pre_spawn_x = 0
        self.pre_spawn_y = 0

        self.coins = 0

        #amount of switches
        self.switch_amount = 0

        #animation
        player_anim_ticks_per_frame = 10
        self.anim_system = gfx.Animation_System({
            "static_right":g.spritesheets["player"].create_animation(0, player_anim_ticks_per_frame),
            "static_left":g.spritesheets["player"].create_animation(1, player_anim_ticks_per_frame),
            "right":g.spritesheets["player"].create_animation(2, player_anim_ticks_per_frame),
            "left":g.spritesheets["player"].create_animation(3, player_anim_ticks_per_frame),
            "climb_right":g.spritesheets["player"].create_animation(4, player_anim_ticks_per_frame),
            "climb_left":g.spritesheets["player"].create_animation(5, player_anim_ticks_per_frame),
            "space_right":g.spritesheets["player"].create_animation(6, player_anim_ticks_per_frame),
            "space_left":g.spritesheets["player"].create_animation(7, player_anim_ticks_per_frame)

        })
        self.anim_system.set_playing("static_left")

    def reset(self):
        self.coins = 0

    def spawn_at(self, spawn_x, spawn_y):
        self.pre_spawn_x = self.x
        self.pre_spawn_y = self.y

        self.x = spawn_x*g.TW
        self.y = spawn_y*g.TH
        self.vx = 0
        self.vy = 0
        self.update_rect()

    def update_tick(self):
        old_vx = self.vx
        super().update_tick()

        #handle sounds
        if abs(old_vx) >= 0.1 and abs(self.vx) <= 0.1:
            sound.play("stop")
            g.sound_dict["move"].stop()

        if not self.is_grounded:
            self.fall_time += 1
        else:
            self.fall_time = 0

        if self.climb_cooldown:
            self.climb_cooldown -= 1

    def update(self, delta_tick):
        self.apply_gravity = not self.climbing

        
        old_is_grounded = self.is_grounded
        super().update(delta_tick)
        new_is_grounded = self.is_grounded
        
        if new_is_grounded and not old_is_grounded:
            sound.play("land")
            if abs(self.vx) > 0.1:
                g.sound_dict["move"].play(loops=-1)

        if not new_is_grounded:
            g.sound_dict["move"].stop()

        #set animation
        if self.climbing:
            if self.climbing.rect.centerx - self.rect.centerx <= 0:
                anim_name = "climb_left"
            else:
                anim_name = "climb_right"

        elif not new_is_grounded:
            if self.direction == "right":
                anim_name = "space_right"
            else:
                anim_name = "space_left"
        
        else:

            if abs(self.vx) <= 0.05:
                if self.direction == "right":
                    anim_name = "static_right"
                else:
                    anim_name = "static_left"
            else:
                if self.direction == "right":
                    anim_name = "right"
                else:
                    anim_name = "left"

        self.anim_system.set_playing(anim_name)


    @property
    def is_on_ladder(self):
        return util.check_collision(self.rect, {"ladder":True})

    def jump(self):
        """
        Make the player jump
        """
        sound.play("jump")
        self.vy = -self.jump_strength

    def draw(self):
        g.screen.blit(self.anim_system.get_frame(), self.rect)

